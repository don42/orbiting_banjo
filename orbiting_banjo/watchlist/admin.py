from django.contrib import admin

from .models import ContentItem, WatchlistItem

admin.site.register(WatchlistItem)
admin.site.register(ContentItem)
