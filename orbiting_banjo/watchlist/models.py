from enum import Enum

from django.db import models
from django.contrib import auth


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return ((x.value, x.name) for x in cls)


class ContentItem(models.Model):
    class ReferenceType(ChoiceEnum):
        DirectLink = 'D'
        Youtube = 'Y'
        MediaCCC = 'C'
        Vimeo = 'V'

    class ContentType(ChoiceEnum):
        Video = 'V'
        Article = 'A'
        Podcast = 'P'
        Music = 'M'

    name = models.CharField(max_length=255)
    content_type = models.CharField(max_length=1, choices=ContentType.choices())
    reference_type = models.CharField(max_length=1, choices=ReferenceType.choices())
    reference = models.CharField(max_length=255)

    @property
    def reference_type_enum(self):
        return self.ReferenceType(self.reference_type)

    @property
    def content_type_enum(self):
        return self.ContentType(self.content_type)

    def __str__(self):
        return f"{self.name}"


class WatchlistItem(models.Model):
    add_date = models.DateTimeField('date added')
    user = models.ForeignKey(auth.get_user_model(), on_delete=models.CASCADE)
    reference = models.ForeignKey(ContentItem, on_delete=models.PROTECT)

    def __str__(self):
        return f"{self.reference.name}:{self.add_date}"
