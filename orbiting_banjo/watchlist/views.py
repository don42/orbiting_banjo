from typing import List

from django.db.models import QuerySet
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render
from django.contrib import auth

from watchlist.models import WatchlistItem
from . import models


def index(request):
    user = auth.get_user(request)
    if user is None:
        return HttpResponseForbidden("Not logged in")
    watchlist: List[WatchlistItem] = list(models.WatchlistItem.objects.order_by('add_date').all())
    context = {
        'watchlist': watchlist,
    }
    return render(request, 'watchlist/index.html', context)
